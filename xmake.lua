add_rules("mode.release", "mode.debug")
set_languages("c++20")

add_requires("glfw", "glew")

target("celoust")
    set_kind("binary")
    add_headerfiles("src/**.h")
    add_files("src/**.cpp")

    if is_plat("windows") then
        add_links("user32")
        add_syslinks("opengl32")
        if is_mode("debug") then
            add_cxflags("/ZI")
            add_mflags("/ZI")
        end
    end

    add_packages("glfw", "glew")