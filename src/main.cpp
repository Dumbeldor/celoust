#include <iostream>
#include "Application.h"
#include "File.h"

using namespace Celoust;

int main(int argc, char** argv)
{
    // todo make an utils for that
    if (argc > 1)
    {
        Celoust::File::SetBasePath(argv[1]);
    }
    else
    {
        std::cerr << "please provide a base path for the assets as an argument." << std::endl;
        return -1;
    }

    Application application;
    if (!application.Init())
        return -1;

    application.Start();
  
    return 0;
}