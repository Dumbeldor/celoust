#pragma once

#include <iostream>
#include <sstream>
#include <map>
#include <chrono>
#include <iomanip>
#include <format>

#define CREATE_LOGGER(category) static Celoust::Logger logger = Celoust::Logger(category);

namespace Celoust
{
    enum class LogLevel
    {
        DEBUG,
        INFO,
        WARN,
        ERR,
    };

    const std::map<LogLevel, const char*> level_colors = {
        {LogLevel::DEBUG,   "\033[90m"},
        {LogLevel::INFO,    "\033[97m"},
        {LogLevel::WARN, "\033[93m"},
        {LogLevel::ERR,   "\033[91m"}
    };

    class Log
    {
    public:
        static void log_message(LogLevel level, const std::string& category, const std::string& message)
        {
            std::stringstream ss;
            ss  << level_colors.at(level)
                << "[" << current_timestamp().str() << "] "
                << "[" << category << "] "
                << message
                << "\033[0m";  // Reset color
            std::cout << ss.str() << std::endl;
        }

    private:
        inline static std::stringstream current_timestamp()
        {
            auto now = std::chrono::system_clock::now();
            auto itt = std::chrono::system_clock::to_time_t(now);
            auto ms = duration_cast<std::chrono::milliseconds>(now.time_since_epoch()) % 1000;

            std::stringstream ss;
            ss << std::put_time(std::localtime(&itt), "%Y-%m-%d %H:%M:%S") << '.' << std::setfill('0') << std::setw(3) << ms.count();
            return ss;
        }
    };

    class Logger {
    public:
        Logger(const std::string&& category) : m_category(std::move(category)) {};

        inline void debug(const std::string& message) const
        {
            Log::log_message(LogLevel::DEBUG, m_category, message);
        }
        inline void info(const std::string& message) const
        {
            Log::log_message(LogLevel::INFO, m_category, message);
        }
        inline void warn(const std::string& message) const
        {
            Log::log_message(LogLevel::WARN, m_category, message);
        }
        inline void err(const std::string& message) const
        {
            Log::log_message(LogLevel::ERR, m_category, message);
        }

    private:
        const std::string m_category;
    };
}