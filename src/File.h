#pragma once

#include <fstream>
#include <iostream>
#include <string>
#include <filesystem>

#include "Log/Log.h"

CREATE_LOGGER("File")

namespace Celoust
{
    class File
    {
    public:

        static void SetBasePath(const std::filesystem::path& path)
        {
            basePath = path;
        }

        static const std::string LoadFileAsString(const std::string& filepath)
        {
            std::string content = "";
            const std::filesystem::path fullPath = basePath / filepath;
            std::ifstream file(fullPath, std::ios::in | std::ios::binary);

            if (!file.is_open())
            {
                logger.err(std::format("Error to read the file : {}", filepath));
                return content;
            }

            file.seekg(0, std::ios::end);
            content.resize(file.tellg());
            file.seekg(0, std::ios::beg);
            file.read(&content[0], content.size());

            file.close();

            return content;
        }

    private:
        static std::filesystem::path basePath;        
    };

}