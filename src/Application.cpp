#include "Application.h"
#include "Log/Log.h"
#include <iostream>
#ifdef _WIN32
#include "Platform/Windows/WindowsPlatform.h"
#endif

CREATE_LOGGER("App")

bool Celoust::Application::Init()
{
#ifdef _WIN32
    m_Platform = std::make_unique<WindowsPlatform>();
#else
    logger.err("Platform not supported");
    return false;
#endif
    if (!m_Platform->platform_create_window(1200, 700, "Celoust"))
    {
        logger.err("Error to create window");
        return false;
    }
    return true;
}

void Celoust::Application::Start()
{
    while (bRunning)
    {
        Update();
    }
}

void Celoust::Application::Update()
{
    if (m_Platform->GetShouldClose())
        bRunning = false;
    m_Platform->platform_update_window();
}
