#pragma once
#include <memory>
#include "Platform/Platform.h"

namespace Celoust
{
    class Application
    {
    public:
        bool Init();
        void Start();

    private:
        void Update();

        std::unique_ptr<Platform> m_Platform;
        bool bRunning = true;
    };
}