#pragma once
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include "../Platform.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>

namespace Celoust
{
    class WindowsPlatform : public Platform
    {

    public:

        WindowsPlatform() = default;
        ~WindowsPlatform();
        virtual bool platform_create_window(int width, int height, const char* title) override;
        virtual void platform_update_window() override;

        std::function<void()> m_CloseCallback;
    private:
        void update_fps_counter();
        static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods);
        static void error_callback(int error, const char* description);
        static void gl_debug_message(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* user);

        GLFWwindow* window;
        // TODO remove
        GLuint vao;
        GLuint shader_programme;
    };
}
#endif