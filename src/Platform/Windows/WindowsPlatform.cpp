#include "WindowsPlatform.h"

#ifdef _WIN32
#include "../../Log/Log.h"
#include <GL/GL.h>
#include "../../File.h"

// todo fix
//CREATE_LOGGER("WinPlatform");

Celoust::WindowsPlatform::~WindowsPlatform()
{
    glfwTerminate();
}

bool Celoust::WindowsPlatform::platform_create_window(int width, int height, const char* title)
{
    glfwSetErrorCallback(error_callback);

    if (!glfwInit())
    {
        logger.err("Error to init GLFW");
        return false;
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_SAMPLES, 4);

    window = glfwCreateWindow(width, height, title, nullptr, nullptr);
    if (!window)
    {
        glfwTerminate();
        logger.err("Error to create window with GLFW");
        return false;
    }
    logger.info("Success to create GLFW window");

    glfwSetKeyCallback(window, key_callback);

    glfwMakeContextCurrent(window);
    logger.info("Success to make GLFW context");

    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK)
    {
        logger.err("Error to init GLEW");
        return false;
    }
    logger.info("Success to init GLEW");
    const GLubyte* renderer = glGetString(GL_RENDERER); // get renderer string
    const GLubyte* version = glGetString(GL_VERSION); // version as a string
    logger.info(std::format("Rendered: {}", *renderer));
    logger.info(std::format("OpenGL version supported {}", *version));

    glEnable(GL_DEPTH_TEST);
    glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    glEnable(GL_DEBUG_OUTPUT);
    glDepthFunc(GL_LESS);

    // TODO remove
    {
        float points[] = {
        0.0f, 0.5f, 0.0f,
        0.5f, -0.5f, 0.0f,
        -0.5f, -0.5f, 0.0f
        };

        GLuint vbo = 0;
        glGenBuffers(1, &vbo);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glBufferData(GL_ARRAY_BUFFER, 9 * sizeof(float), points, GL_STATIC_DRAW);

        vao = 0;
        glGenVertexArrays(1, &vao);
        glBindVertexArray(vao);
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vbo);
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, nullptr);

        const char* vertex_shader = File::LoadFileAsString("assets/shaders/quad.vert").c_str();

        const char* fragment_shader =
            "#version 400\n"
            "out vec4 frag_colour;"
            "void main() {"
            "  frag_colour = vec4(0.9, 0.2, 0.5, 1.0);"
            "}";

        GLuint vs = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vs, 1, &vertex_shader, nullptr);
        glCompileShader(vs);
        GLint result;
        glGetShaderiv(vs, GL_COMPILE_STATUS, &result);
        if (!result)
        {
            char errorLog[512];
            glGetShaderInfoLog(vs, sizeof(errorLog), nullptr, errorLog);
            logger.err(std::format("Error to compile vertex_shader : {}", errorLog));

        }

        GLuint fs = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fs, 1, &fragment_shader, nullptr);
        glCompileShader(fs);

        shader_programme = glCreateProgram();
        glAttachShader(shader_programme, fs);
        glAttachShader(shader_programme, vs);
        glLinkProgram(shader_programme);
    }

    glDebugMessageCallback(gl_debug_message, nullptr);

    return true;
}

void Celoust::WindowsPlatform::platform_update_window()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glUseProgram(shader_programme);
    glBindVertexArray(vao);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    glfwSwapBuffers(window);
    glfwPollEvents();

    if (glfwWindowShouldClose(window))
        bShouldClose = true;

    update_fps_counter();
}

void Celoust::WindowsPlatform::update_fps_counter()
{
    static double previous_seconds = glfwGetTime();
    static int frame_count;
    double current_seconds = glfwGetTime();
    double elapsed_seconds = current_seconds - previous_seconds;

    if (elapsed_seconds > 0.25)
    {
        previous_seconds = current_seconds;
        double fps = (double)frame_count / elapsed_seconds;
        char tmp[128];
        sprintf(tmp, "Celoust @ fps: %.2f", fps);
        glfwSetWindowTitle(window, tmp);
        frame_count = 0;
    }
    frame_count++;
}

void Celoust::WindowsPlatform::key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, GLFW_TRUE);
}

void Celoust::WindowsPlatform::error_callback(int error, const char* description)
{
    std::stringstream ss;
    ss << "GLFW Error: " << description;
    logger.err(ss.str());
}
void Celoust::WindowsPlatform::gl_debug_message(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* user)
{
    if (severity == GL_DEBUG_SEVERITY_HIGH || severity == GL_DEBUG_SEVERITY_MEDIUM || severity == GL_DEBUG_SEVERITY_LOW)
    {
        logger.err(std::format("OpenGL Error: {}", message));
    }
    else
    {
        logger.debug(message);
    }
}
#endif