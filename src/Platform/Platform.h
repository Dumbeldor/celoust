#pragma once
#include <functional>

namespace Celoust
{
    class Platform
    {
    public:
        virtual bool platform_create_window(int width, int height, const char* title) = 0;
        virtual void platform_update_window() = 0;
        bool GetShouldClose() const { return bShouldClose; }
    protected:
        bool bShouldClose = false;
    };
}